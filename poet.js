//Require line is imported to read user input
const readline = require("readline");
//Creating user input and output interface for further use
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
//Creating delay variable to be used
const delay = ms => new Promise(res => setTimeout(res, ms));

class ComputerNode {
    constructor(nodeLabel){
        this.nodeLabel = nodeLabel;
        this.time = 1;
    }

    randomTime(){
        this.time = parseFloat(Math.random()).toFixed(2);
        return this.time;
    }
}

rl.question("How many nodes do you want to test ? ", async function(number) {
    var arr = []
    for (var i = 0; i < number; i++){
        arr.push(`NODE-${i+1}`)
    }

    console.log("Generating Nodes, please wait\n")

    var delayTime = `${number}00`;
    await delay(delayTime);

    console.log("=======================================================")
    console.log("NODES:")
    var times = []
    arr.forEach( (a)=>{
        var nd = new ComputerNode(a);
        console.log("Random time for: "+a + ": "+nd.randomTime())
        times.push(nd)
    })
    var smallest = new ComputerNode("");
    times.forEach((t)=>{
        if (t.time < smallest.time){
            smallest = t;
        }
    })
    console.log("=======================================================")
    console.log("MINER:")
    console.log(smallest.nodeLabel + " with time : " + smallest.time) 
    console.log("=======================================================")
    rl.close();
});