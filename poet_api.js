const express = require('express')
const app = express()
const port = 1234

class ComputerNode {
    constructor(nodeLabel){
        this.nodeLabel = nodeLabel;
        this.time = 1;
    }

    randomTime(){
        this.time = parseFloat(Math.random()).toFixed(2);
        return this.time;
    }
}

app.get('/', (req, res) => {
    if (req.query['nodes']){
        var number = req.query['nodes'];
        if (number > 50){
            res.send("We are not using super computers... Nodes should be less than 50 for this simulation")
        }
        else {
            var randomtime_details_output = []
            var output = {}
            var arr = []
            for (var i = 0; i < number; i++){
                arr.push(`NODE-${i+1}`)
            }
            var times = []
            arr.forEach( (a)=>{
                var nd = new ComputerNode(a);
                randomtime_details_output.push("Random time for: "+a + ": "+nd.randomTime())
                times.push(nd)
            })
            var smallest = new ComputerNode("");
            times.forEach((t)=>{
                if (t.time < smallest.time){
                    smallest = t;
                }
            })
            output['nodes'] = randomtime_details_output
            output['winner'] = smallest.nodeLabel + " with time : " + smallest.time
            res.end(JSON.stringify(output,null, 2))
        }
    }
    else {
        res.send("Send me the right variable dude... {nodes}")
    }
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})