var os = require('os');
const io = require("socket.io-client");
const socket = io("http://192.168.0.106:3000");
var hostname = os.hostname()
var time = 0
socket.emit("puzzle-status", {
    message: hostname
});
socket.on("selected",function(data){
    if (data.message === '1'){
        //Selected
        console.log("you are selected for mining")
        time = parseFloat(Math.random()).toFixed(2);
        socket.emit("generated-time", {
            message: time,
            socketId: data.id,
            ip: hostname
        });
    }
    else {
        //Not Selected
        console.log("you are not selected for mining")
    }
})
socket.on("winner",function(data){
    console.log(data)
})
