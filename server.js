const express = require("express");
const socket = require("socket.io");
const PORT = 3000;
const app = express();
const server = app.listen(PORT, function () {
    console.log(`Listening on port ${PORT}`);
});
const io = socket(server);

var nodes = []
var smallestTime = {
    message: '1000'
}
var blocks = []
var toBePicked = 0

var percentage = 100 //Set to negative to initialize it 
// while(percentage % 10 != 0){
//     percentage = Math.floor(Math.random() * 50) + 1;
// }
console.log("Percentage: " + percentage)

io.on("connection", function (socket) {
    nodes.push(socket.id);
    console.log("Node count: " + nodes.length)
    socket.on('disconnect', function() {
       var i = nodes.indexOf(socket.id);
       nodes.splice(i, 1);
       console.log("Node count: " + nodes.length)
    });
    
    if (nodes.length == 2){
        // Shuffle array
        const shuffled = nodes.sort(() => 0.5 - Math.random());
        // console.log("sh"+shuffled)
        toBePicked = (nodes.length) * (percentage/100)
        // Get sub-array of first n elements after shuffled 
        let selected = shuffled.slice(0, Math.ceil(toBePicked));
        nodes.forEach(element => {
            var index = selected.indexOf(element)
            if (index != -1){
                //Found
                io.to(element).emit("selected",{
                    message: "1",
                    id: element
                })
            }
            else {
                //Not Found
                io.to(element).emit("selected",{
                    message: "0"
                })
            }
        });
    }

    //Reply from client if selected
    socket.on('generated-time', function(data) {
        blocks.push(data)
        if (blocks.length === Math.ceil(toBePicked)){
            blocks.forEach((t)=>{
                console.log(t)
                if (parseFloat(t.message) < parseFloat(smallestTime.message)){
                    smallestTime = t;
                    console.log("amad")
                }
            })
            console.log("Winner: " + JSON.stringify(smallestTime))
            io.to(smallestTime.socketId).emit("winner",{
                message: "You won yayyyy!"
            })
        }
     });
});