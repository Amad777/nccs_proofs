//Importing Socket Client
const io = require("socket.io-client");
//Setting server socket ip and port
const socket = io("http://192.168.0.104:3000");
//Importing SHA256 Library
const sha256 = require('sha256');
//Importing Process Library
var process = require('process'); 
//After importing process library, we are using it to check ID of our process running on OS
if (process.pid) {
    console.log('This process is your pid ' + process.pid);
}
//Our puzzle string
var puzzleString = ("amad");
//Converting our puzzle string to sha256
var guess = sha256(puzzleString);

var st = ""
var answer = ""
var index = 0 //Used for loop iterations to show index where answer is found

//makeid is our own declared function, it can be named anything
//this function is used to guess the puzzle
//it takes a parameter [length] that is used to tell function, length of our puzzleString
function makeid() {
    var result           = '';
    var characters       = '\sABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890+=-_)"(*&^%$#@!`~/?.>,<;:]}[{ '; //Characters involved in guessing
    var charactersLength = characters.length;
    for ( var i = 0; i < 1; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength)); //charAt is a builtin function, that finds which character is at given index
    }
    return result;
}

console.log("working...")
var startTime = new Date().getTime()/1000;
var startMillis = new Date().getMilliseconds();
var guessedAnswer = ""
var guessIndex = 0
while (st != guess){
    index += 1
    answer = makeid()
    if(puzzleString[guessIndex] == answer){
        guessedAnswer =  guessedAnswer.concat(answer)
        guessIndex += 1;
        if(guessedAnswer == puzzleString){
            break
        }
    }
}
var endTime = new Date().getTime()/1000;

var endMillis = new Date().getMilliseconds();
var differenceInTime = endMillis-startMillis;
console.log("Answer:        "+ guessedAnswer +  "     In:     "+ parseFloat(differenceInTime) + "MilliSeconds")
socket.emit("puzzle-status", {
    message: "Answer:        "+ guessedAnswer + "     In:     "+ parseFloat(differenceInTime) + "MilliSeconds"
});
