from numba import jit, cuda

import numpy as np
# to measure exec time
from timeit import default_timer as timer   

import math
import random
  
def makeid(length):
    guess = "A"
    st = ""
    while st != guess:
        try:
            st = ""
            characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
            chractersLength = len(characters)
            for i in range(1):
                st += characters[math.floor( random.random() * chractersLength)]
        except:
            continue

# normal function to run on cpu
def func():
    print("Amad")
    # guess = "amad"
    # st = ""
    # while st != guess:
    #     try:
    #         st = ""
    #         characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
    #         chractersLength = len(characters)
    #         for i in range(len(guess)):
    #             st += characters[math.floor( random.random() * chractersLength)]
    #     except:
    #         continue  
    # print("String: " + guess)  
  
# function optimized to run on gpu 
@cuda.jit
def func2():
    guess = "amad"
    st = ""
    while st != guess:
        try:
            st = ""
            characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
            chractersLength = len(characters)
            for i in range(len(guess)):
                st += characters[math.floor( random.random() * chractersLength)]
        except:
            continue
    print("String: " + guess)
    
    

if __name__=="__main__":
    n = 10000000                            
    a = np.ones(n, dtype = np.float64)
    b = np.ones(n, dtype = np.float32)
    # func()
    # print("without GPU:", timer()-start)    
      
    start = timer()
    func()
    print("with GPU:", timer()-start)