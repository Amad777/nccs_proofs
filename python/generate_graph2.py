import matplotlib.pyplot as plt
import numpy
  
names = []
marks = []
ind = 0
  
f = open('../output_diff3_100times.txt','r')
for row in f:
    row = row.split(' ')
    names.append(str(ind))
    marks.append(float(row[0]))
    ind += 1

# plt.bar(names, marks, color = 'g', label = 'File Data')
  
# plt.xlabel('Our readings', fontsize = 12)
# plt.ylabel('Marks', fontsize = 12)
  
# plt.title('Our Readings', fontsize = 20)
# plt.legend()
# plt.show()
print("Standard Deviation   " + str(numpy.std(marks)))
print("Mean " + str(numpy.mean(marks)))
print("Total seconds taken " + str(numpy.sum(marks)))
formula = 100 * 0.001 + 2 * 0.001 + 0 + numpy.sum(marks)
print("Formula: ")
print(str(formula))