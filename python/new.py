import numpy as np
from numba import cuda

@cuda.jit
def my_kernel(str_array, check_str, length, lines, result):

    col,line = cuda.grid(2)
    pos = (line*(length+1))+col
    if col < length and line < lines:  # Check array boundaries
        if str_array[pos] != check_str[col]:
            result[line] = 0

def main():
    guess = "amad"
    st = ""
    i = 0
    while st != guess:
        try:
            i=i+1
            st = ""
            characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
            chractersLength = len(characters)
            for i in range(len(guess)):
                st += characters[math.floor( random.random() * chractersLength)]
        except:
            continue
    line_length = i
    line_count = i.shape[0]/(line_length+1)
    res = np.ones(line_count)
    threadsperblock = (32,32)
    blocks_x = (line_length/32)+1
    blocks_y = (line_count/32)+1
    blockspergrid = (blocks_x,blocks_y)
    my_kernel[blockspergrid, threadsperblock](a, b, line_length, line_count, res)
    print("String: " + guess)

if __name__ == '__main__':
        main()