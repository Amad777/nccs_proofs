import matplotlib.pyplot as plt
import numpy
  
names = []
marks = []
ind = 0
  
f = open('../output_diff4_100times.txt','r')
for row in f:
    row = row.split(' ')
    names.append(str(ind))
    marks.append(float(row[0]))
    ind += 1

formula = 100 * 0.001 + 2 * 0.001 + 1 + numpy.sum(marks)
print("Formula: ")
print(str(formula))
fig = plt.figure(figsize =(10, 7))
 
# Creating plot
plt.boxplot(marks)
 
# show plot
plt.show()